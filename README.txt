
image_enhanced_scaling.module

Image Enhanced Scaling replaces the image scaling function used by the 
image module with one that gives a better image quality. This is achieved 
by applying a sharpness filter to the image derivatives and converting 
them to a suitable color profile.

The HEAD version of this module is currently not used. This directory is 
therefore empty. Please use either the DRUPAL-5 or the DRUPAL-4-7 branch.
